showCursor( true )

local sW, sH = guiGetScreenSize()

local radialItems = {
    "measure", "tools", "scissors", "brush", "pencil",
}

local menu = {
    isOpen = true,
    radius = 200,
    angleStep = math.pi * 2 / #radialItems,
    rotShift = math.pi / 180 * 15,
    posX = sW / 2, 
    posY = sH / 2
}

local transition = {}
transition.target = nil
transition.startTime = getTickCount()
transition.duration = 250
transition.easingFunction = "InOutQuad"
transition.direction = "normal"  -- or "reverse"


local function isCursorInsideCircle( cursorX, cursorY, circleX, circleY, circleRadius )
    local distance = math.sqrt( (cursorX - circleX) ^ 2 + (cursorY - circleY) ^ 2 )
    return distance < circleRadius
end


function drawRadialMenu ()
    if not menu.isOpen then return end

    local cursorX, cursorY = getCursorPosition()
    cursorX, cursorY = sW * cursorX, sH * cursorY

    dxDrawRectangle ( 0, 0, sW, sH, 0xff2b67ac )

    for i = 1, #radialItems do
        local x = math.floor( menu.posX + math.sin( menu.angleStep * (i - 1) + menu.rotShift ) * menu.radius )
        local y = math.floor( menu.posY - math.cos( menu.angleStep * (i - 1) + menu.rotShift ) * menu.radius )
        
        -- при наведении курсора на элемент в меню
        if isCursorInsideCircle( cursorX, cursorY, x, y, 80 ) then
            if transition.target ~= i or transition.direction == "reverse" then
                transition.target = i
                transition.direction = "normal"
                transition.startTime = getTickCount()
                transition.duration = 250
                transition.easingFunction = "InOutQuad"
            end

            local now = getTickCount()
            local elapsedTime = now - transition.startTime
            local progress = elapsedTime / transition.duration

            if progress > 1 then
                progress = 1
            end
            
            local hoverTime = getEasingValue( progress, transition.easingFunction )
            local shellSize = hoverTime * 178
            dxDrawImage ( x - shellSize / 2, y - shellSize / 2, shellSize, shellSize, "images/dotted_shell.png" )
            
            dxDrawImage ( x - 80, y - 80, 160, 160, "images/circle.png" )

            local oy = hoverTime * 30
            dxDrawText ( utf8.gsub(radialItems[i], "^%l", utf8.upper), x - 45, y + oy, x + 45, y + oy + 24, tocolor( 0, 0, 0, hoverTime * 255 ), hoverTime * 1.2, "default", "center", "center" )

            dxDrawImage ( x - 45, y - 45 - hoverTime * 12, 90, 90, "images/"..radialItems[i]..".png" )
        
        -- при отведении курсора
        elseif transition.target == i then
            local now = getTickCount()

            if transition.direction ~= "reverse" then
                transition.direction = "reverse"
                transition.duration = now - transition.startTime
                transition.startTime = now
                transition.easingFunction = "InOutQuad"
            end

            local elapsedTime = now - transition.startTime
            local progress = elapsedTime / transition.duration

            if progress > 1 then
                progress = 1
                transition.target = nil
            end

            local hoverTime = getEasingValue( progress, transition.easingFunction )
            local shellSize = ( 1 - hoverTime ) * 178
            dxDrawImage ( x - shellSize / 2, y - shellSize / 2, shellSize, shellSize, "images/dotted_shell.png" )

            dxDrawImage ( x - 80, y - 80, 160, 160, "images/circle.png" )
            dxDrawImage ( x - 45, y - 45 - (1 - hoverTime) * 12, 90, 90, "images/"..radialItems[i]..".png" )

        else
            dxDrawImage ( x - 80, y - 80, 160, 160, "images/circle.png" )
            dxDrawImage ( x - 45, y - 45, 90, 90, "images/"..radialItems[i]..".png" )
        end

    end
end
addEventHandler ( "onClientRender", root, drawRadialMenu )


addEventHandler( "onClientResourceStart", getRootElement(),
    function ( )
        bindKey( "tab", "down", function() 
            menu.isOpen = not menu.isOpen
        end )
        outputChatBox ( "Нажмите 'tab' для переключения отображения меню", 67, 160, 71 )
    end
)

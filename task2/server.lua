dbCon = dbConnect ("mysql", "host=127.0.0.1;port=3306;dbname=testdb;charset=utf8", 
    "testdbuser", "testdbuserpass", "multi_statements=1")


if dbCon then
    dbExec ( 
        dbCon,
        [[CREATE TABLE IF NOT EXISTS `accounts` (
            id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
            serial CHAR(32) UNIQUE NOT NULL DEFAULT '',
            nickName VARCHAR(32) NOT NULL DEFAULT '',
            vehId SMALLINT UNSIGNED NOT NULL DEFAULT 0,
            carPlateNumber VARCHAR(16) NULL DEFAULT NULL,
            createdDate TIMESTAMP DEFAULT CURRENT_TIMESTAMP
        ) DEFAULT CHARSET=utf8]]
    )

    dbExec (dbCon, [[ALTER TABLE `accounts` ADD `money` INT NOT NULL DEFAULT 0 AFTER `createdDate`;]])
end



local playerVehicles = {}
local playerVehiclesData = {}

addEventHandler ( "onResourceStart", root, function( res ) 
    if res ~= getThisResource () then return end

    if not dbCon then 
        outputDebugString("Warning! Db connection failed!", 1)
        return
    end
    
    AddRequestHandler( "onPlayerBoughtVehicle", 
    function( Response, vehId, carPlateNumber )

        if type(vehId) ~= "number" or type(carPlateNumber) ~= "string" or not VehInfo[vehId] then 
            Response( false, "Данной машины нет в продаже" )
            return 
        end

        local data = playerVehiclesData[client]
        if not data then 
            Response( false, "Ошибка: не найден аккаунт" )
            return 
        end

        if data.vehId > 0 then
            Response( false, "У вас уже есть машина" )
            return 
        end

        local veh = VehInfo[vehId]
        if data.money < veh.cost then
            Response( false, "Не достаточно средств для покупки" )
            return 
        end

        carPlateNumber = utf8.match( carPlateNumber, "%a%d%d%d%a%a%d%d%d" )
        if not carPlateNumber then
            Response( false, "Номер машины не соответствует формату х000хх000" )
            return
        end

        dbQuery (function(queryHandle, player)

            if not isElement (player) then dbFree (queryHandle) return end
    
            local result, numRows, lastInsertId = dbPoll(queryHandle, 0)
            if not result then
                local errCode, errMsg = numRows, lastInsertId
                outputDebugString ("Check carPlateNumber failed. Error code: " .. tostring(errCode).. ", error message: " .. tostring(errMsg), 1)
                return
            end

            --iprint( result )

            if next(result) then
                Response( false, "Указанный номер машины занят. Выберите другой." )
                return
            end

            if dbExec (
                dbCon, 
                "UPDATE `accounts` SET `vehId` = ?, `carPlateNumber` = ?, `money` = `money` - ? WHERE serial = ?;",
                vehId, carPlateNumber, veh.cost, getPlayerSerial( player )
            ) then
                data.vehId = vehId
                data.carPlateNumber = carPlateNumber
                data.money = data.money - veh.cost

                setPlayerMoney( player, data.money )

                if isElement( playerVehicles[player] ) then destroyElement( playerVehicles[player] ) end
                local veh = VehInfo[vehId]
                local x, y, z = getElementPosition( player )
                playerVehicles[player] = createVehicle ( veh.modelId, x + 2.2, y + 2.2, z + 0.5, 0, 0, 0 )
                
                Response( true, "Машина куплена" )
                return
            end

            Response( false, "Ошибка автосалона. Приходите позже." )
            return
        end,
        {client}, dbCon, [[SELECT 1
        FROM `accounts`
        WHERE `carPlateNumber`=?;
        ]], carPlateNumber)
    end)

        
    AddRequestHandler( "onPlayerSellOwnVehicle", 
    function( Response )
        local vehicle = getPedOccupiedVehicle ( client )
        if not vehicle or getPedOccupiedVehicleSeat ( client ) ~= 0 then
            Response( false, "Вы должны быть за рулем своего авто." )
            return
        end

        if vehicle ~= playerVehicles[client] then
            Response( false, "Это авто вам не принадлежит." )
            return
        end
        
        local data = playerVehiclesData[client]
        if not data then 
            Response( false, "Ошибка: не найден аккаунт" )
            return 
        end

        if not data.vehId or data.vehId == 0 then 
            Response( false, "Транспорт не найден" )
            return 
        end

        local vehInfo = getVehicleInfo( vehicle )

        if not vehInfo then
            Response( false, "Этот транспорт нельзя продать." )
            return
        end

        local vehcost = math.floor(vehInfo.cost * 0.7)

        dbExec( dbCon, 
            "UPDATE `accounts` SET `vehId` = 0, `carPlateNumber` = NULL, `money` = `money` + ? WHERE serial = ?;", 
            vehcost, getPlayerSerial( client )
        )

        Response( true, "Авто продано." )
        data.vehId = 0
        data.carPlateNumber = nil
        data.money = data.money + vehcost
        setPlayerMoney( client, data.money )

        if isElement( playerVehicles[client] ) then
            destroyElement( playerVehicles[client] )
            playerVehicles[client] = nil
        end

        return
    end)


    AddRequestHandler( "onPlayerChangeCarPlateNumber", 
    function( Response, newCarPlateNumber )
        local vehicle = getPedOccupiedVehicle ( client )
        if not vehicle or getPedOccupiedVehicleSeat ( client ) ~= 0 then
            Response( false, "Вы должны быть за рулем своего авто." )
            return
        end

        if vehicle ~= playerVehicles[client] then
            Response( false, "Это авто вам не принадлежит." )
            return
        end
        
        local data = playerVehiclesData[client]
        if not data then 
            Response( false, "Ошибка: не найден аккаунт" )
            return 
        end

        if not data.vehId or data.vehId == 0 then 
            Response( false, "Транспорт не найден" )
            return 
        end

        local vehInfo = getVehicleInfo( vehicle )

        if not vehInfo then
            Response( false, "Для этого транспорта смена номеров не предусмотрена." )
            return
        end

        newCarPlateNumber = utf8.match( newCarPlateNumber, "%a%d%d%d%a%a%d%d%d" )
        if not newCarPlateNumber then
            Response( false, "Номер машины не соответствует формату х000хх000" )
            return
        end

        if data.carPlateNumber == newCarPlateNumber then
            Response( false, "Этот номер уже установлен на вашем авто" )
            return
        end

        dbQuery (function(queryHandle, player)

            if not isElement (player) then dbFree (queryHandle) return end
    
            local result, numRows, lastInsertId = dbPoll(queryHandle, 0)
            if not result then
                local errCode, errMsg = numRows, lastInsertId
                outputDebugString ( "Check carPlateNumber failed. Error code: " .. tostring(errCode).. ", error message: " .. tostring(errMsg), 1 )
                Response( false, "Ошибка проверки номера. Попробуйте позже." )
                return
            end

            if next( result ) then
                Response( false, "Указанный номер машины занят. Выберите другой." )
                return
            end
            
            dbExec( dbCon, 
                "UPDATE `accounts` SET carPlateNumber = ? WHERE serial = ?;",
                newCarPlateNumber,
                getPlayerSerial( player )
            )

            Response( true, "Новый номер установлен." )
            data.carPlateNumber = newCarPlateNumber

            return
        end,
        {client}, dbCon, [[SELECT 1
        FROM `accounts`
        WHERE `carPlateNumber`=? AND `serial` != ?;
        ]], newCarPlateNumber, getPlayerSerial( client ))
    end)
end )


function onPlayerJoinHandler ( )
    dbQuery (function(queryHandle, player)

        if not isElement (player) then dbFree (queryHandle) return end

        local result, numRows, lastInsertId = dbPoll(queryHandle, 0)
        if not result then
            local errCode, errMsg = numRows, lastInsertId
            outputDebugString ("Fetch player data from db failed. Error code: " .. tostring(errCode).. ", error message: " .. tostring(errMsg), 1)
            return
        end

        if next( result ) then
            local data = result[1]
            playerVehiclesData[player] = data
            if data.vehId > 0 and VehInfo[data.vehId] then
                local veh = VehInfo[data.vehId]
                local x, y, z = getElementPosition( player )
                playerVehicles[player] = createVehicle ( veh.modelId, x + 2.2, y + 2.2, z + 0.5, 0, 0, 0 )
            end
            if data.money then
                setPlayerMoney( player, data.money, true )
            end
        else
            dbExec (
                dbCon, 
                "INSERT INTO `accounts` (serial, nickName, money) VALUES (?, ?, 15000);",
                getPlayerSerial(player), getPlayerName(player)
            )
            playerVehiclesData[player] = { 
                serial = getPlayerSerial(player),  
                nickName = getPlayerName(player),  
                money = 15000, 
                vehId = 0,
            }
            setPlayerMoney( player, 15000 )
        end
    end,
    {source}, dbCon, [[SELECT 
        `id`, 
        `serial`, 
        `nickName`, 
        `vehId`, 
        `carPlateNumber`,
        `money`
    FROM `accounts`
    WHERE `serial`=?;
    ]], getPlayerSerial(source))

    spawnPlayer ( source, 14, 9, 5, 0, math.random (0, 288), 0, 0 )
    fadeCamera ( source, true )
    setCameraTarget ( source, source )
end
addEventHandler ( "onPlayerJoin", root, onPlayerJoinHandler, true, "low-100" )


addEventHandler ("onPlayerQuit", root, function ()
    if isElement( playerVehicles[source] ) then destroyElement( playerVehicles[source] ) end
    playerVehicles[source] = nil
    playerVehiclesData[source] = nil
end)



-----------------------------------
------------ for debug ------------

-- dbExec (  
--     dbCon,
--     [[
--         DROP TABLE IF EXISTS accounts;
--     ]]
-- )

-- dbExec (  
--     dbCon,
--     [[
--         UPDATE accounts SET vehId = 0, carPlateNumber = NULL;
--     ]]
-- )

-- addEventHandler ( "onPlayerJoin", root, function()
--     local account = getAccount ( "'", "'" )
--     logIn ( source, account, "'" )
--     setPlayerScriptDebugLevel(source, 3)
-- end, true, "low-1999")

-- addEventHandler ( "onResourceStart", root, function( res ) 
--     if res ~= getThisResource () then return end
--     setTimer(triggerEvent, 1000, 1, "onPlayerJoin", getRandomPlayer())
--     print("resource loaded!!")
-- end, true, "low")
-----------------------------------
-----------------------------------
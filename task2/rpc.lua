--RPC.lua
if localPlayer then
    CONST_SERVER_CALLBACK_TIMEOUT = 5000

	local REQUESTS = {}

	Request = {
		onResponse = function( self, handler_fn )
			self.handler_fn = handler_fn
			return self
		end,
		onSuccess = function( self, handler_fn )
			self.success_handler_fn = handler_fn
			return self
		end,
		onError = function( self, handler_fn )
			self.error_handler_fn = handler_fn
			return self
		end,
		onTimeout = function( self, handler_fn )
			self.timeout_handler_fn = handler_fn
			return self
		end,
	}

	function RequestServer( data_key, ... )
		if next( REQUESTS ) and REQUESTS[ data_key ] then
			killTimer( REQUESTS[ data_key ].timeout_timer )
		end

		triggerServerEvent( "RequestServer", resourceRoot, data_key, ... )

		REQUESTS[ data_key ] = {
			timeout_timer = setTimer( ResponseDataTimeout_handler, CONST_SERVER_CALLBACK_TIMEOUT, 1, data_key ),
		}
		setmetatable( REQUESTS[ data_key ], { __index = Request } )

		return REQUESTS[ data_key ]
	end

	function ResponseDataTimeout_handler( data_key )
		if not REQUESTS[ data_key ] then return end

		local request = REQUESTS[ data_key ]

		REQUESTS[ data_key ] = nil

		if request.timeout_handler_fn then
			request.timeout_handler_fn()
		end
	end

	function ResponseData_handler( data_key, result, msg, ... )
		if not REQUESTS[ data_key ] then return end
		local request = REQUESTS[ data_key ]

		REQUESTS[ data_key ] = nil
		killTimer( request.timeout_timer )

		if request.handler_fn then
			request.handler_fn( result, msg, ... )
		else
			if result == true then
				if request.success_handler_fn then
					request.success_handler_fn( msg, ... )
				end
			elseif result == false then
				if request.error_handler_fn then
					request.error_handler_fn( msg, ... )
				end
			end
		end
	end
	addEvent( "ResponseData", true )
	addEventHandler( "ResponseData", root, ResponseData_handler )

else

    local REQUEST_HANDLERS = {}

    function AddRequestHandler( data_key, handler_fn )
        REQUEST_HANDLERS[ data_key ] = handler_fn
    end
	onRequest = AddRequestHandler

    addEvent( "RequestServer", true )
    addEventHandler( "RequestServer", root, function( data_key, ... )
        if not REQUEST_HANDLERS[ data_key ] then return end

        local client = client
		local is_responded, async = false, false

        local result, msg, arg1, arg2, arg3 = REQUEST_HANDLERS[ data_key ]( function( ... )
			if async and not isElement( client ) then return end
			triggerClientEvent( client, "ResponseData", resourceRoot, data_key, ... )
			is_responded = true
        end, ... )

		if not is_responded and result ~= nil then
            triggerClientEvent( client, "ResponseData", resourceRoot, data_key, result, msg, arg1, arg2, arg3 )
		else
			async = true
		end
    end)

end

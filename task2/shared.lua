local _vehInfo = {
    -- NAME                  MODEL ID      COST       
    {"Banshee",               429,         3000,      },
    {"Bullet",                541,         5000,      },
    {"Cheetah",               415,         7700,      },
    {"Comet",                 480,         7232,      },
    {"Elegy",                 562,         9192,      },
}

VehInfo = {}
for i = 1, #_vehInfo do
    local veh = _vehInfo[i]
    table.insert( VehInfo, {
        id = i,
        name = veh[1],
        modelId = veh[2],
        cost = veh[3],
    })
end


function getVehicleInfo( vehicle ) 
    local modelId = getElementModel(vehicle)
    if not modelId then return false end
    local vehInfo
    for i = 1, #VehInfo do
        local veh = VehInfo[i]
        if veh.modelId == modelId then
            vehInfo = veh
            break
        end
    end
    return vehInfo
end

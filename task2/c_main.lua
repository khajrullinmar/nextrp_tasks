DGS = exports.dgs

gUIelems = {}

local sW, sH = guiGetScreenSize()

function showInfoBox( message, callback )

    if isElement( gUIelems.infoBox ) then
        destroyElement( gUIelems.infoBox )
    end

    local message = tostring( message or "" )

    local rndRect = DGS:dgsCreateRoundRect( 20, false, tocolor(31, 31, 31, 255) )
    local w, h = 400, 300
    local box = DGS:dgsCreateImage( sW/2 - w/2, sH/2 - h/2, w, h, rndRect, false )

    DGS:dgsLabelSetHorizontalAlign(
        DGS:dgsCreateLabel ( 0, 80, w, 30, "Информация", false, box ),
        "center"
    )

    DGS:dgsLabelSetHorizontalAlign(
        DGS:dgsCreateLabel ( 0, h/2 - 15, w, 30, message, false, box ),
        "center"
    )

    local okBtn = DGS:dgsCreateButton( w/2 - 45, h - 60, 90, 30, "Понятно", false, box )
    addEventHandler ( "onDgsMouseClick", okBtn, function()
        if source ~= okBtn then return end
        destroyElement ( box )
        okBtn = nil
        if type( callback ) == "function" then
            callback()
        end
    end )

    return box
end


local function showEnterCarPlateNumberModal( veh )

    if isElement( gUIelems.carSalonUi ) then
        DGS:dgsSetEnabled( gUIelems.carSalonUi, false )
    end

    local rndRect = DGS:dgsCreateRoundRect( 20, false, tocolor(31, 31, 31, 255) )
    local w, h = 400, 300
    local modal = DGS:dgsCreateImage( 0, 0, w, h, rndRect, false )
    
    DGS:dgsLabelSetHorizontalAlign(
        DGS:dgsCreateLabel ( 0, 80, w, 30, "Введите желаемый номер машины", false, modal ),
        "center"
    )

    local editBox = DGS:dgsCreateEdit( w/2 - 150, 120, 300, 40, "", false, modal )
    DGS:dgsSetProperty( editBox, "placeHolder", "в формате: х000хх000" )

    DGS:dgsLabelSetHorizontalAlign(
        DGS:dgsCreateLabel ( 0, 170, w, 30, "где х - любая буква, 0 - любая цифра", false, modal ),
        "center"
    )
    
    local buyBtn = DGS:dgsCreateButton( w/2 - 95, h - 60, 90, 30, "Купить", false, modal )
    local cancelBtn = DGS:dgsCreateButton( w/2 + 5, h - 60, 90, 30, "Отмена", false, modal )

    addEventHandler ( "onDgsMouseDown", modal, function()
        if source == buyBtn then
            local text = DGS:dgsGetText(editBox)
            if type( text ) ~= "string" then return end
            local carPlateNumber = utf8.match( text, "%a%d%d%d%a%a%d%d%d" )
            if not carPlateNumber then
                DGS:dgsSetEnabled( modal, false )
                gUIelems.infoBox = showInfoBox(
                    "Номер машины не соответствует формату х000хх000",
                    function() DGS:dgsSetEnabled( modal, true ) end
                )
                return
            end

            DGS:dgsSetEnabled( modal, false )

            RequestServer( 
                "onPlayerBoughtVehicle", veh.id, carPlateNumber
            ):onResponse(function( result, data )
                DGS:dgsSetEnabled( modal, true ) 
                DGS:dgsSetProperty( modal, "visible", false )
                destroyElement( modal )
                modal = nil
                buyBtn = nil
                cancelBtn = nil
                if isElement( gUIelems.carSalonUi ) then
                    DGS:dgsSetEnabled( gUIelems.carSalonUi, true )
                end
                if result == true then
                    gUIelems.infoBox = showInfoBox( "Вы купили "..veh.name )
                else
                    local errorMsg = data or "Ошибка при покупке машины"
                    gUIelems.infoBox = showInfoBox( errorMsg )
                end
                return
            end):onTimeout(function()
                gUIelems.infoBox = showInfoBox( "Чтото пошло не так. Попробуйте позже." )
                return
            end)
        elseif source == cancelBtn then
            DGS:dgsSetProperty( modal, "visible", false )
            destroyElement( modal )
            modal = nil
            buyBtn = nil
            cancelBtn = nil
            if isElement( gUIelems.carSalonUi ) then
                DGS:dgsSetEnabled( gUIelems.carSalonUi, true )
            end
        end
    end)

    DGS:dgsBringToFront( modal )
    DGS:dgsCenterElement( modal )
end


if not DGS:dgsEasingFunctionExists("slidein") then
    DGS:dgsAddEasingFunction("slidein",[[
    --Predefined variable
    -- progress: from 0 to 1 , indicates the progress of the whole animation
    -- setting: a table {propertyName,targetValue,initialValue}
    -- self: the dgs element
    -- propertyTable: property table of the dgs element
        local animTime = getEasingValue(progress, "InOutQuad")
        return {(setting[2][1] - setting[3][1]) * animTime + setting[3][1], setting[3][2]}
    ]])
end


function showCarSalonUi( state )
    if state then
        if isElement( gUIelems.carSalonUi ) then
            destroyElement( gUIelems.carSalonUi )
        end

        local window = DGS:dgsCreateWindow( sW/2 - 250, sH/2 - 200, 500, 370, "Aвтосалон", false )
        gUIelems.carSalonUi = window
        addEventHandler ( "onDgsWindowClose", window, function() showCursor( false ) end )

        local scrollBox = DGS:dgsCreateScrollPane( 760, 340, 500, 370, false, window )
        DGS:dgsSetProperty( scrollBox, "scrollBarState", {nil, false} )

        local itemSize = 150
        local itemCountPerLine = 3
        local gap = 10
        local x, y = gap, gap
        for i = 1, #VehInfo do
            local veh = VehInfo[i]
            local rndRect = DGS:dgsCreateRoundRect( 20, false, tocolor( 0, 81, 137 ) )
            local itemBox = DGS:dgsCreateImage( x, y, itemSize, itemSize, rndRect, false, scrollBox )
            DGS:dgsAnimTo(itemBox, "absPos", {500, y}, "slidein", 700, 0, false, true)

            DGS:dgsCreateLabel ( 10, 10, 100, 30, veh.name, false, itemBox )
            DGS:dgsLabelSetHorizontalAlign(
                DGS:dgsCreateLabel ( itemSize - 80 - 10, 10, 80, 30, veh.cost.." $", false, itemBox ),
                "right"
            )
            DGS:dgsCreateImage( itemSize/2 - 67/2, itemSize/2 - 20/2, 67, 20, "images/"..veh.name..".png", false, itemBox )
            local btn = DGS:dgsCreateButton( itemSize - 90, itemSize - 30, 90, 30, "Купить", false, itemBox )
            
            x = x + itemSize + gap
            
            if i % itemCountPerLine == 0 then
                x = gap
                y = (i / itemCountPerLine) * itemSize + (i / itemCountPerLine + 1) * gap
            end
            setElementData( btn, "itemId", i )
        end

        function commonClickHandler ( btn, x, y )
            if getElementType ( source ) ~= "dgs-dxbutton" then return end

            local itemId = getElementData(source, "itemId")
            if type( itemId ) ~= "number" then return end

            if not VehInfo[ itemId ] then return end

            local veh = VehInfo[ itemId ]
            showEnterCarPlateNumberModal( veh )
        end
        addEventHandler ( "onDgsMouseDown", scrollBox, commonClickHandler )

        addEventHandler( "onDgsMouseEnter", scrollBox, function(aX, aY)
            if getElementType( source ) == "dgs-dximage" and DGS:dgsGetParent( source ) == scrollBox then
                DGS:dgsRoundRectSetColor( DGS:dgsGetProperty(source, "image"), tocolor( 8, 100, 111 ) )
            end
        end)

        addEventHandler( "onDgsMouseLeave", scrollBox, function(aX, aY)
            if getElementType( source ) == "dgs-dximage" and DGS:dgsGetParent( source ) == scrollBox then
                DGS:dgsRoundRectSetColor( DGS:dgsGetProperty(source, "image"), tocolor( 0, 81, 137 ) )
            end
        end)

        DGS:dgsCenterElement( scrollBox )

        showCursor( true )
    else
        if isElement( gUIelems.carSalonUi ) then
            destroyElement( gUIelems.carSalonUi )
        end
        if isElement( gUIelems.infoBox ) then
            destroyElement( gUIelems.infoBox )
        end
        gUIelems.carSalonUi = nil
        gUIelems.infoBox = nil
        showCursor( false )
    end
end


function showCarSellUi( state )
    if state then
        if isElement( gUIelems.carSellUi ) then
            showCarSellUi( false )
        end

        showCursor( true )

        local vehicle = getPedOccupiedVehicle ( localPlayer )
        if not vehicle or getPedOccupiedVehicleSeat ( localPlayer ) ~= 0 then
            gUIelems.infoBox = showInfoBox( "Вы должны быть за рулем своего авто", function() showCarSellUi( false ) end )
            return
        end

        local vehInfo = getVehicleInfo( vehicle )

        if not vehInfo then
            gUIelems.infoBox = showInfoBox( "Этот транспорт нельзя продать ", function() showCarSellUi( false ) end )
            return
        end
    
        local rndRect = DGS:dgsCreateRoundRect( 20, false, tocolor(31, 31, 31, 255) )
        local w, h = 400, 300
        local uiBox = DGS:dgsCreateImage( 0, 0, w, h, rndRect, false )
        gUIelems.carSellUi = uiBox
        
        DGS:dgsLabelSetHorizontalAlign(
            DGS:dgsCreateLabel ( 0, 80, w, 30, "Желаете продать машину за "..math.floor(vehInfo.cost * 0.7).."$ ?", false, uiBox ),
            "center"
        )
        
        local confirmBtn = DGS:dgsCreateButton( w/2 - 95, h - 60, 90, 30, "Продаю", false, uiBox )
        local cancelBtn = DGS:dgsCreateButton( w/2 + 5, h - 60, 90, 30, "Отмена", false, uiBox )
    
        addEventHandler ( "onDgsMouseDown", uiBox, function()
            if source == confirmBtn then
                DGS:dgsSetEnabled( uiBox, false )

                RequestServer( 
                    "onPlayerSellOwnVehicle"
                ):onResponse(function( result, data )
                    DGS:dgsSetEnabled( uiBox, true ) 
                    DGS:dgsSetProperty( uiBox, "visible", false )
                    uiBox = nil
                    confirmBtn = nil
                    cancelBtn = nil
                    if result == true then
                        gUIelems.infoBox = showInfoBox( "Вы успешно продали свое авто", function() showCarSellUi( false ) end )
                    else
                        local errorMsg = data or "Ошибка при продаже авто"
                        gUIelems.infoBox = showInfoBox( errorMsg, function() showCarSellUi( false ) end )
                    end
                    return
                end):onTimeout(function()
                    gUIelems.infoBox = showInfoBox( "Чтото пошло не так. Попробуйте позже.", function() showCarSellUi( false ) end )
                    return
                end)
            elseif source == cancelBtn then
                DGS:dgsSetProperty( uiBox, "visible", false )
                uiBox = nil
                confirmBtn = nil
                cancelBtn = nil
                showCarSellUi( false )
            end
        end)
    
        DGS:dgsBringToFront( uiBox )
        DGS:dgsCenterElement( uiBox )
    else
        if isElement( gUIelems.carSellUi ) then
            destroyElement( gUIelems.carSellUi )
        end
        if isElement( gUIelems.infoBox ) then
            destroyElement( gUIelems.infoBox )
        end
        gUIelems.infoBox = nil
        gUIelems.carSellUi = nil
        showCursor( false )
    end
end


function showEditPlateNumberUi( state )

    if state then
        if isElement( gUIelems.editPlateNumberUi ) then
            showEditPlateNumberUi( false )
        end

        showCursor( true )

        local vehicle = getPedOccupiedVehicle ( localPlayer )
        if not vehicle or getPedOccupiedVehicleSeat ( localPlayer ) ~= 0 then
            gUIelems.infoBox = showInfoBox( "Вы должны быть за рулем своего авто", function() showEditPlateNumberUi( false ) end )
            return
        end

        local vehInfo = getVehicleInfo( vehicle )

        if not vehInfo then
            gUIelems.infoBox = showInfoBox( "Этот транспорт не пригоден для замены номера", function() showEditPlateNumberUi( false ) end )
            return
        end

        local rndRect = DGS:dgsCreateRoundRect( 20, false, tocolor(31, 31, 31, 255) )
        local w, h = 400, 300
        local uiBox = DGS:dgsCreateImage( 0, 0, w, h, rndRect, false )
        gUIelems.editPlateNumberUi = uiBox
        
        DGS:dgsLabelSetHorizontalAlign(
            DGS:dgsCreateLabel ( 0, 80, w, 30, "Введите новый номер машины", false, uiBox ),
            "center"
        )

        local editBox = DGS:dgsCreateEdit( w/2 - 150, 120, 300, 40, "", false, uiBox )
        DGS:dgsSetProperty( editBox, "placeHolder", "в формате: х000хх000" )

        DGS:dgsLabelSetHorizontalAlign(
            DGS:dgsCreateLabel ( 0, 170, w, 30, "где х - любая буква, 0 - любая цифра", false, uiBox ),
            "center"
        )
        
        local confirmBtn = DGS:dgsCreateButton( w/2 - 95, h - 60, 90, 30, "Применить", false, uiBox )
        local cancelBtn = DGS:dgsCreateButton( w/2 + 5, h - 60, 90, 30, "Отмена", false, uiBox )

        addEventHandler ( "onDgsMouseDown", uiBox, function()
            if source == confirmBtn then
                local text = DGS:dgsGetText(editBox)
                if type( text ) ~= "string" then return end
                local carPlateNumber = utf8.match( text, "%a%d%d%d%a%a%d%d%d" )
                if not carPlateNumber then
                    DGS:dgsSetEnabled( uiBox, false )
                    gUIelems.infoBox = showInfoBox(
                        "Номер машины не соответствует формату х000хх000",
                        function() DGS:dgsSetEnabled( uiBox, true ) end
                    )
                    return
                end

                DGS:dgsSetEnabled( uiBox, false )

                RequestServer( 
                    "onPlayerChangeCarPlateNumber", carPlateNumber
                ):onResponse(function( result, data )
                    DGS:dgsSetEnabled( uiBox, true ) 
                    DGS:dgsSetProperty( uiBox, "visible", false )
                    destroyElement( uiBox )
                    uiBox = nil
                    confirmBtn = nil
                    cancelBtn = nil
                    if result == true then
                        gUIelems.infoBox = showInfoBox( "Новый номер установлен",  function() showEditPlateNumberUi( false ) end )
                    else
                        local errorMsg = data or "Ошибка замены номера"
                        gUIelems.infoBox = showInfoBox( errorMsg,  function() showEditPlateNumberUi( false ) end )
                    end
                    return
                end):onTimeout(function()
                    gUIelems.infoBox = showInfoBox( "Чтото пошло не так. Попробуйте позже.",  function() showEditPlateNumberUi( false ) end )
                    return
                end)
            elseif source == cancelBtn then
                DGS:dgsSetProperty( uiBox, "visible", false )
                showEditPlateNumberUi( false )
                uiBox = nil
                confirmBtn = nil
                cancelBtn = nil
            end
        end)

        DGS:dgsBringToFront( uiBox )
        DGS:dgsCenterElement( uiBox )
    else
        if isElement( gUIelems.editPlateNumberUi ) then
            destroyElement( gUIelems.editPlateNumberUi )
        end
        if isElement( gUIelems.infoBox ) then
            destroyElement( gUIelems.infoBox )
        end
        gUIelems.editPlateNumberUi = nil
        gUIelems.infoBox = nil
        showCursor( false )
    end
end



addEventHandler( "onClientResourceStart", getRootElement(),
    function ( )
        -- автосалон
        local carSalonMarker = createMarker ( 0, 0, 2.0, "cylinder", 1.5, 255, 255, 0, 170 )
        local text = DGS:dgsCreate3DText( 0, 0, 0.3, "АвтоСалон", 0xFFFFFFFF )
        DGS:dgsSetProperty( text, "fadeDistance", 20 )
        DGS:dgsSetProperty( text, "shadow",{1, 1, tocolor(0, 0, 0, 255), true} )
        DGS:dgs3DTextAttachToElement( text, carSalonMarker, 0, 0, 1.9 )

        local function carSalonMarkerHitHandler ( hitPlayer, matchingDimension )
            showCarSalonUi( true )
        end
        addEventHandler ( "onClientMarkerHit", carSalonMarker, carSalonMarkerHitHandler )

        local function carSalonMarkerLeaveHandler ( leavingPlayer, matchingDimension )
            showCarSalonUi( false )
        end
        addEventHandler ( "onClientMarkerLeave", carSalonMarker, carSalonMarkerLeaveHandler )


        -- продажа машины
        local carSellMarker = createMarker ( 0, 8, 2.0, "cylinder", 1.5, 255, 255, 0, 170 )
        local text = DGS:dgsCreate3DText( 0, 0, 0.3, "Продать машину", 0xFFFFFFFF )
        DGS:dgsSetProperty( text, "fadeDistance", 20 )
        DGS:dgsSetProperty( text, "shadow",{1, 1, tocolor(0, 0, 0, 255), true} )
        DGS:dgs3DTextAttachToElement( text, carSellMarker, 0, 0, 1.9 )

        local function carSellMarkerHitHandler ( hitPlayer, matchingDimension )
            showCarSellUi( true )
        end
        addEventHandler ( "onClientMarkerHit", carSellMarker, carSellMarkerHitHandler )

        local function carSellMarkerLeaveHandler ( leavingPlayer, matchingDimension )
            showCarSellUi( false )
        end
        addEventHandler ( "onClientMarkerLeave", carSellMarker, carSellMarkerLeaveHandler )


        -- смена номера машины
        local plateNumberMarker = createMarker ( 0, 16, 2.0, "cylinder", 1.5, 255, 255, 0, 170 )
        local text = DGS:dgsCreate3DText( 0, 0, 0.3, "Изменить номер", 0xFFFFFFFF )
        DGS:dgsSetProperty( text, "fadeDistance", 20 )
        DGS:dgsSetProperty( text, "shadow",{1, 1, tocolor(0, 0, 0, 255), true} )
        DGS:dgs3DTextAttachToElement( text, plateNumberMarker, 0, 0, 1.9 )

        local function plateNumberMarkerHitHandler ( hitPlayer, matchingDimension )
            showEditPlateNumberUi( true )
        end
        addEventHandler ( "onClientMarkerHit", plateNumberMarker, plateNumberMarkerHitHandler )

        local function plateNumberMarkerLeaveHandler ( leavingPlayer, matchingDimension )
            showEditPlateNumberUi( false )
        end
        addEventHandler ( "onClientMarkerLeave", plateNumberMarker, plateNumberMarkerLeaveHandler )
    end
)


function checkIsDgsResourceRunning ()
    local dgsResource = getResourceFromName ( "dgs" )
    if ( not dgsResource or getResourceState( dgsResource ) ~= "running" ) then
        dxDrawRectangle ( sW/3.8, sH/3.8, sW/2.2, sH/6, tocolor ( 0, 0, 0, 150 ) )
        dxDrawText ( "Ресурс DGS не запущен ", sW/3.5, sH/3.6, sW, sH, tocolor ( 255, 255, 255, 255 ), 2.5)
        dxDrawLine ( sW/3.6, sH/3, sW/1.83, sH/3, tocolor ( 255, 255, 255, 255 ), 2 )
    else
        removeEventHandler( "onClientRender", root, checkIsDgsResourceRunning )
    end
end
addEventHandler ( "onClientRender", root, checkIsDgsResourceRunning )